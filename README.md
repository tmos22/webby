# webby

This is a simple golang webserver to just test golang and the echo framework.

(Also because the marquee tag is the best)

## Files
### /cmd
The main package/function to be executed to run the server

### /web
Contains static files and templates for rendering the pages

### /pkg
Contains the modules used throughout this service